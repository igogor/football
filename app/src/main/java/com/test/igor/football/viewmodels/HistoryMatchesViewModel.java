package com.test.igor.football.viewmodels;

import com.test.igor.football.dto.matches.Matches;
import com.test.igor.football.repository.MatchesRepository;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HistoryMatchesViewModel extends ViewModel {
    private MutableLiveData<Matches> matches;

    public MutableLiveData<Matches> getMatches(boolean isSchedule) {
        if (matches == null) {
            matches = new MutableLiveData<>();
        }
        MatchesRepository repository = new MatchesRepository();
        matches = repository.getMatches(isSchedule);
        return matches;
    }
}
