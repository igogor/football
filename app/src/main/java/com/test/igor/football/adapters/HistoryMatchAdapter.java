package com.test.igor.football.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.igor.football.R;
import com.test.igor.football.dto.matches.Competition;
import com.test.igor.football.dto.matches.FullTime;
import com.test.igor.football.dto.matches.Match;
import com.test.igor.football.fragments.HistoryFragment;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryMatchAdapter extends RecyclerView.Adapter<HistoryMatchAdapter.MatchHolder> {
    private Context context;
    private List<Match> matchList;
    private Competition competition;
    private HistoryFragment.CallbackListener listener;

    public HistoryMatchAdapter(Context context, List<Match> matchList, Competition competition, HistoryFragment.CallbackListener listener) {
        this.context = context;
        this.matchList = matchList;
        this.competition = competition;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MatchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.matches_list_item, parent, false);
        return new MatchHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchHolder holder, int position) {
        holder.bind(matchList.get(position));
    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }

    class MatchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.nameTextView)
        TextView name;
        @BindView(R.id.teamTextView)
        TextView team;
        @BindView(R.id.scoreTextView)
        TextView score;
        private Match match;
        private HistoryFragment.CallbackListener listener;

        MatchHolder(@NonNull View itemView, HistoryFragment.CallbackListener listener) {
            super(itemView);
            this.listener = listener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bind(Match match) {
            this.match = match;
            name.setText(competition.getName());
            team.setText(match.getHomeTeam().getName() +
                    " - " +
                    match.getAwayTeam().getName());
            FullTime fullTime = match.getScore().getFullTime();
            score.setText(new StringBuilder().append(fullTime.getHomeTeam())
                    .append(":")
                    .append(fullTime.getAwayTeam()).toString());
        }

        @Override
        public void onClick(View v) {
            listener.onMatchClick(match);
        }
    }
}
