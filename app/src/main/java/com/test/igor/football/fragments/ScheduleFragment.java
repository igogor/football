package com.test.igor.football.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.igor.football.adapters.ScheduleMatchAdapter;
import com.test.igor.football.dto.matches.Competition;
import com.test.igor.football.dto.matches.Match;
import com.test.igor.football.viewmodels.HistoryMatchesViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

public class ScheduleFragment extends BaseFragment {
    private List<Match> matchesList = new ArrayList<>();
    private ScheduleMatchAdapter adapter;
    private HistoryMatchesViewModel matchesViewModel;
    private Competition competition;

    public static ScheduleFragment newInstance() {
        return new ScheduleFragment();
    }

    public ScheduleFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        matchesViewModel = ViewModelProviders.of(this).get(HistoryMatchesViewModel.class);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isOnline)
            matchesViewModel.getMatches(true).observe(this, matches -> {
                matchesList.clear();
                matchesList.addAll(matches.getMatches());
                competition = matches.getCompetition();
                updateUI();
            });
    }

    private void updateUI() {
        if (adapter == null) {
            adapter = new ScheduleMatchAdapter(this.getActivity(), matchesList, competition);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }
}
