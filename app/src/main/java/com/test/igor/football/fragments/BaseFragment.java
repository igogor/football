package com.test.igor.football.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.igor.football.R;
import com.test.igor.football.helpers.NetworkHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BaseFragment extends Fragment {
    @BindView(R.id.list)
    protected RecyclerView recyclerView;
    @BindView(R.id.messageText)
    TextView infoText;
    protected boolean isOnline;
    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isOnline = NetworkHelper.isOnline(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (isOnline) {
            infoText.setVisibility(View.GONE);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setHasFixedSize(true);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            infoText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            infoText.setText(R.string.info_no_connection);
        }
        return view;
    }

    @Override
    public void onDestroy() {
        unbinder = null;
        super.onDestroy();
    }
}
