package com.test.igor.football.net.retrofit.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request request = original.newBuilder()
                .header("X-Auth-Token", "cefcff85deb142b69ede74b243f71305")
                .build();
        return chain.proceed(request);
    }
}
