package com.test.igor.football.fragments;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.test.igor.football.R;
import com.test.igor.football.helpers.Consts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.content.Context.MODE_PRIVATE;
import static com.test.igor.football.helpers.Consts.DEVICE;
import static com.test.igor.football.helpers.Consts.LOCATION;

public class SecondFragment extends Fragment {
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.device)
    TextView device;
    Unbinder unbinder;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    public SecondFragment() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_second, container, false);
        unbinder = ButterKnife.bind(this, view);

        SharedPreferences preferences = getActivity().getApplication().getSharedPreferences(Consts.MY_PREFERENCES, MODE_PRIVATE);
        String json = preferences.getString(LOCATION, null);
        Location loc = new Gson().fromJson(json, Location.class);
        if (loc != null) {
            String sb = "Lat = " +String.valueOf(loc.getLatitude()) +
                    "\n" +
                    "Lon = " +String.valueOf(loc.getLongitude());
            location.setText(sb);
        } else {
            location.setText(getString(R.string.info_no_location));
        }
        device.setText("Model - "+preferences.getString(DEVICE, null));
        return view;
    }

    @Override
    public void onDestroy() {
        unbinder = null;
        super.onDestroy();
    }
}
