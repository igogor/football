package com.test.igor.football;

import com.test.igor.football.fragments.SecondFragment;

import androidx.fragment.app.Fragment;

public class SecondActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return SecondFragment.newInstance();
    }

}
