package com.test.igor.football.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.igor.football.adapters.HistoryMatchAdapter;
import com.test.igor.football.dto.matches.Competition;
import com.test.igor.football.dto.matches.Match;
import com.test.igor.football.viewmodels.HistoryMatchesViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

public class HistoryFragment extends BaseFragment {
    private List<Match> matchesList = new ArrayList<>();
    private HistoryMatchAdapter adapter;
    private HistoryMatchesViewModel matchesViewModel;
    private Competition competition;
    private CallbackListener listener;

    public interface CallbackListener {
        void onMatchClick(Match match);
    }

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    public HistoryFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof CallbackListener) {
            listener = (CallbackListener) context;
        }
        else {
            throw new ClassCastException(context.toString() + " must implement CallbackListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        matchesViewModel = ViewModelProviders.of(this).get(HistoryMatchesViewModel.class);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isOnline)
            matchesViewModel.getMatches(false).observe(this, matches -> {
                matchesList.clear();
                matchesList.addAll(matches.getMatches());
                competition = matches.getCompetition();
                updateUI();
            });
    }

    @Override
    public void onDestroy() {
        listener = null;
        super.onDestroy();
    }

    private void updateUI() {
        if (adapter == null) {
            adapter = new HistoryMatchAdapter(this.getActivity(), matchesList, competition, listener);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }
}
