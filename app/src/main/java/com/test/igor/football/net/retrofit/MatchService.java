package com.test.igor.football.net.retrofit;

import com.test.igor.football.dto.matches.Matches;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MatchService {
    @GET("v2/competitions/{id}/matches")
    Call<Matches> getMatchesByCompetition(@Path("id") String id,
                                          @Query("dateFrom") String dateFrom,
                                          @Query("dateTo") String dateTo);

}
