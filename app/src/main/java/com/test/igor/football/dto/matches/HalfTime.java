package com.test.igor.football.dto.matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class HalfTime {
    @SerializedName("homeTeam")
    @Expose
    private int homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private int awayTeam;

    public HalfTime() {}

    public HalfTime(int homeTeam, int awayTeam) {
        super();
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public int getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(int homeTeam) {
        this.homeTeam = homeTeam;
    }

    public int getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(int awayTeam) {
        this.awayTeam = awayTeam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HalfTime halfTime = (HalfTime) o;
        return Objects.equals(homeTeam, halfTime.homeTeam) &&
                Objects.equals(awayTeam, halfTime.awayTeam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(homeTeam, awayTeam);
    }

    @Override
    public String toString() {
        return "HalfTime{" +
                "homeTeam=" + homeTeam +
                ", awayTeam=" + awayTeam +
                '}';
    }
}
