package com.test.igor.football.net.retrofit;

import android.util.Log;

import com.test.igor.football.net.retrofit.interceptor.HeaderInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceBuilder {
    private static final String URL = "https://api.football-data.org/";

    private static HttpLoggingInterceptor logInterceptor
            = new HttpLoggingInterceptor(message -> Log.d("Interceptor", message))
                .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient okHttp = new OkHttpClient.Builder()
            .addInterceptor(new HeaderInterceptor())
            .addInterceptor(logInterceptor)
            .build();

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(URL)
            .client(okHttp)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S buildService(Class<S> serviceType) {
        return retrofit.create(serviceType);
    }
}
