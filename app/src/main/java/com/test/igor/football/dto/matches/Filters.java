package com.test.igor.football.dto.matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Filters {
    @SerializedName("dateFrom")
    @Expose
    private String dateFrom;
    @SerializedName("dateTo")
    @Expose
    private String dateTo;
    @SerializedName("permission")
    @Expose
    private String permission;

    public Filters() { }

    public Filters(String dateFrom, String dateTo, String permission) {
        super();
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.permission = permission;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Filters filters = (Filters) o;
        return Objects.equals(dateFrom, filters.dateFrom) &&
                Objects.equals(dateTo, filters.dateTo) &&
                Objects.equals(permission, filters.permission);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateFrom, dateTo, permission);
    }

    @Override
    public String toString() {
        return "Filters{" +
                "dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", permission='" + permission + '\'' +
                '}';
    }
}
