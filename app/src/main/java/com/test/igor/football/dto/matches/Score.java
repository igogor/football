package com.test.igor.football.dto.matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Score {
    @SerializedName("winner")
    @Expose
    private String winner;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("fullTime")
    @Expose
    private FullTime fullTime;
    @SerializedName("halfTime")
    @Expose
    private HalfTime halfTime;
    @SerializedName("extraTime")
    @Expose
    private ExtraTime extraTime;
    @SerializedName("penalties")
    @Expose
    private Penalties penalties;

    /**
     * No args constructor for use in serialization
     *
     */
    public Score() {}

    public Score(String winner, String duration, FullTime fullTime, HalfTime halfTime, ExtraTime extraTime, Penalties penalties) {
        super();
        this.winner = winner;
        this.duration = duration;
        this.fullTime = fullTime;
        this.halfTime = halfTime;
        this.extraTime = extraTime;
        this.penalties = penalties;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public FullTime getFullTime() {
        return fullTime;
    }

    public void setFullTime(FullTime fullTime) {
        this.fullTime = fullTime;
    }

    public HalfTime getHalfTime() {
        return halfTime;
    }

    public void setHalfTime(HalfTime halfTime) {
        this.halfTime = halfTime;
    }

    public ExtraTime getExtraTime() {
        return extraTime;
    }

    public void setExtraTime(ExtraTime extraTime) {
        this.extraTime = extraTime;
    }

    public Penalties getPenalties() {
        return penalties;
    }

    public void setPenalties(Penalties penalties) {
        this.penalties = penalties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return Objects.equals(winner, score.winner) &&
                Objects.equals(duration, score.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(winner, duration);
    }

    @Override
    public String toString() {
        return "Score{" +
                "winner='" + winner + '\'' +
                ", duration='" + duration + '\'' +
                ", fullTime=" + fullTime +
                ", halfTime=" + halfTime +
                ", extraTime=" + extraTime +
                ", penalties=" + penalties +
                '}';
    }
}
