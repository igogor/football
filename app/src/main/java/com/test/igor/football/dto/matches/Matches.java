package com.test.igor.football.dto.matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Matches {
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("filters")
    @Expose
    private Filters filters;
    @SerializedName("competition")
    @Expose
    private Competition competition;
    @SerializedName("matches")
    @Expose
    private List<Match> matches = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Matches() {}

    public Matches(int count, Filters filters, Competition competition, List<Match> matches) {
        super();
        this.count = count;
        this.filters = filters;
        this.competition = competition;
        this.matches = matches;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Filters getFilters() {
        return filters;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }
}
