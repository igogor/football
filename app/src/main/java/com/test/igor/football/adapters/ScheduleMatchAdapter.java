package com.test.igor.football.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.igor.football.R;
import com.test.igor.football.dto.matches.Competition;
import com.test.igor.football.dto.matches.Match;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleMatchAdapter extends RecyclerView.Adapter<ScheduleMatchAdapter.MatchHolder> {
    private Context context;
    private List<Match> matchList;
    private Competition competition;

    public ScheduleMatchAdapter(Context context, List<Match> matchList, Competition competition) {
        this.context = context;
        this.matchList = matchList;
        this.competition = competition;
    }

    @NonNull
    @Override
    public MatchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.matches_schedule_list_item, parent, false);
        return new MatchHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchHolder holder, int position) {
        holder.bind(matchList.get(position));
    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }

    class MatchHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTextView)
        TextView name;
        @BindView(R.id.teamTextView)
        TextView team;
        @BindView(R.id.scoreTextView)
        TextView score;

        MatchHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        void bind(Match match) {
            name.setText(competition.getName());
            team.setText(match.getHomeTeam().getName() +
                    " - " +
                    match.getAwayTeam().getName());
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            try {
                Date date = format.parse(match.getUtcDate());
                CharSequence res = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm a", date);
                score.setText(res);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
