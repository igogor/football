package com.test.igor.football.repository;

import com.test.igor.football.dto.matches.Matches;
import com.test.igor.football.net.retrofit.MatchService;
import com.test.igor.football.net.retrofit.ServiceBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatchesRepository {

    private MatchService service;

    public MatchesRepository() {
        service = ServiceBuilder.buildService(MatchService.class);
    }

    public MutableLiveData<Matches> getMatches(boolean isSchedule) {
        MutableLiveData<Matches> data = new MutableLiveData<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dateNow = new Date();
        String nowDate = sdf.format(dateNow.getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateNow);
        String secondDate;

        Call<Matches> call;
        if (isSchedule) {
            calendar.add(Calendar.DAY_OF_MONTH, 5);
            secondDate = sdf.format(calendar.getTime());
            call = service.getMatchesByCompetition("2019", nowDate, secondDate);
        } else {
            calendar.add(Calendar.DAY_OF_MONTH, -5);
            secondDate = sdf.format(calendar.getTime());
            call = service.getMatchesByCompetition("2019", secondDate, nowDate);
        }
        call.enqueue(new Callback<Matches>() {
            @Override
            public void onResponse(Call<Matches> call, Response<Matches> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Matches> call, Throwable t) {

            }
        });
        return data;
    }

}
